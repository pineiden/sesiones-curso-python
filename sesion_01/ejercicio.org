#+TITLE: Juanito sale a marchar

Juanito se levanta temprano, le cuenta a su madre que saldrá a marchar por los
derechos de la educación. La mamá se preocupa, porque hace un poco de frio, se
pregunta que tan abrigado deberá salir según la temperatura.

| prenda   | temperatura [°C] |
|----------+------------------|
| polera   | t > 25           |
| polerón  | 15 < t < 25      |
| chaleco  | 5  < t < 15      |
| chaqueta | t < 5            |


a) Crear el diagrama de flujo que representará la elección de la prenda según
cada caso.

b) ¿Cómo saber que tan abrigado debería salir Juanito según el criterio de su madre
según cada día de la tabla siguiente? Completar la tabla
 
| día       | temperatura media [°C] | prenda |
|-----------+------------------------+--------|
| lunes     |                     12 |        |
| martes    |                     17 |        |
| miércoles |                     22 |        |
| jueves    |                     27 |        |
| viernes   |                      8 |        |

Temas a estudiar.

- el conjunto de valores posibles
- comparadores numéricos
- condicionales
- interacción computador-usuario
- listas de elementos
- resultado
- diagrama de flujo

c) Juanito, además, es dirigente estudiantil. Justo este mes tiene varias
actividades en la calle y su querida madre lo tiene todo preparado, contabilizó
que debe lavar cada prenda según su uso.

| prenda   | usos |
|----------+------|
| polera   |    1 |
| polerón  |    3 |
| chaleco  |    5 |
| chaqueta |    8 |

¿Cuántas veces deberá lavar cada prenda según el calendario registrado en el
archivo *calendario.txt*?

Considerar que /Juanito/ solo tiene 1 prenda de cada una.

Temas a estudiar

- contadores
- diccionarios
- rutas a archivos
- lectura de archivos
- conversión de tipos de datos (parsing)

d) Desafío

Cuantas veces lavará cada prenda si tiene más de una según la siguiente tabla.

| prenda   | cantidad |
|----------+----------|
| polera   |        5 |
| polerón  |        3 |
| chaleco  |        2 |
| chaqueta |        1 |




