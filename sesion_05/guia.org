#+TITLE: Elementos de Geometría

Tiempo : 5h

Este ejercicio consiste en diseñar y modelar diferentes elementos geométricos,
identificando características comunes e implementándolas en código /python/ y
luego utilizarlos para resolver algunos problemas.

Consta de dos partes que traban de manera dependiente. Para resolver los
problemas de la segunda parte será necesario ir implementando y afinando
detalles del código de la primera parte.

Aprenderemos a crear un módulo con un set de herramientas que proveen los
objetos necesarios para desarrollar código límpio y comprensible.

* Módulo de figuras 

I) Punto :: es el elemento base que indica una posición en el espacio, para
planos (2D) necesita 2 puntos (x,y) para volúmenes (3D) necesitará 3 puntos
(x,y,z).

Se caracteriza por no tener perímetro, área o volumen.

Implementar el objeto *punto*.

II) Línea Recta :: es una serie de puntos conectados que al se puede construir
con dos pares de puntos.

Implementar el objeto *línea*

III) Triángulo :: es una figura de tres líneas con orientación diferente, cuyos
puntos de intersección son los vértices. Comprende también tres ángulos.

Implementar el objeto *triángulo*

IV) Cuadrado/Cuadrilátero :: es una figur
a que se compone de cuatro líneas
rectas cuya intersección son los vértices.

Implementar el objeto *cuadrilatero*

V) Polígono Regular :: para una cantidad de lados N determinada, un polígono
regular es la figura resultante de la intersección de N líneas rectas.

Implementar el objeto *cuadrilatero*

VI) Círculo :: es la figura que representa la unión de puntos equidistante a un
punto central.

Implementar el objeto *círculo*

VII) Consolidación :: encontrar elemento comunes característicos e implementar
una clase base que guíe la construcción de los elementos anteriores.


https://gitlab.com/Raquelbracho77/figuras

https://gitlab.com/Raquelbracho77/generar_figuras


* Resolución de problemas

Modulos recomendados para usar

json
pprint
rich
click
math

I) Realizar lo siguiente:

- script que pregunte nombre de figura
- pregunte parámetros
- calcular área y perímetro
- crear diccionario con sus valores (de entrada  y resultados)
- acumular en una lista
- terminar con END o FIN
- Guardar lista como json en un archivo .json
- mostrar la lista de diccionarios (imprimir)

II) Calcular y comparar figuras.

- Encontrar un tríangulo cuya área sea igual al área de un octágono. 
- Encontrar un cuadrado cuyo perímetro se tres veces el perímetro de un
  dodecágono. ¿Qué area tendrían ambos? 
- Encontrar un tríangulo equilátero, un cuadrado, un hexagono y un heptágono que
  tengan el mismo perímetro, retornar el lado de cada figura. 
- Encontrar el heptágono cuya área sea tres veces la de un pentágono de lado 3.
  Retornar sus lados. 
- Encontrar la relación entre un poligono regular circunscrito en un circulo de
  radio R, tal que el area del poligono sea un 99.99% igual a la del circulo.
  Encontrar el N de lados que satisfaga la relación 



