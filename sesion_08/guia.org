#+TITLE: Secuencias y fractales


Las secuencias son expresiones que representan una serie de valores en que
cambia uno o más parámetros. Se representan mediante una fórmula que se expresa
de manera general.

Así, la secuencia {1,2,3,4,5....100,101,....1000} se puede expresar mediante

\begin{equation}
a_n = n
\end{equation}

La secuencia de números pares {0,2,4,6,8...,100,102,...} por 

\begin{equation}
a_n = 2*n
\end{equation}

O la de impares por

\begin{equation}
a_n = 2*n + 1
\end{equation}

También será posible abstraer patrones que se observen del comportamiento de
ciertos fenómenos que se observen secuenciales.

I) Implementar las secuencias para la suceción de números, números pares e
impares.

II) Implementar el promedio y desviación estándar de una serie de valores.

III) Implementar fibonnacci

IV) Implementar una función que retorne el resutado de la serie geométrica.

V) Dado un tríangulo de base 1 unidad, se contiene a sí mismo. Uno de base 2
unidades, contiene 4 triángulos de base 1. Uno de base 3 contiene 9 tríangulos
de base 1. ¿Cuantos triángulos habrán para una base 100 o 500?

VI) Las placas de vehículos puedes ser combinaciones de letras y dígitos. Se te
encarga gestionar la base de datos de estas para que sean únicas ligadas a un
solo vehículo. Solo crea series de 3 letras y 3 dígitos.
