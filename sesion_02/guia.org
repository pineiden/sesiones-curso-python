#+TITLE: Cicletada

Juanito y su grupo de amigos están entrenando para la competencia de ciclismo
que sera el próximo mes. Debes cumplir tres tramos.


#+begin_src plantuml :file ruta.png
ditaa
+--------+   +--------+    +--------+    +--------+
|        +-->+        +--->|        |--->|        |
|        |   |        |    |San     |    |        |
|Santiago|   |Rancagua|    |Fernando|    |Curicó  |
|     {d}|   |        |    |        |    |        |
+---+----+   +--------+    +--------+    +--------+
    :            ^             ^              ^
    |    88km    |     52km    |     53km     |
    +------------+-------------+--------------+
#+end_src

#+RESULTS:
[[file:ruta.png]]



<--------50km-------------><-----------30km-----------------><--------45km-------->
+-------------------------+----------------------------------+--------------------+
Santiago                Rancagua                        San Fernando           Curicó

Entre *Santiago* y *Rancagua* demoran, según la tabla.

| Nombre  | Tiempo [hr] |
|---------+-------------|
| Juanito |           6 |
| David   |        6:30 |
| Lucía   |        7:00 |
| Martín  |        6:20 |
| Mariela |        5:50 |

a) ¿Cuál sería la rapidez de cada uno?


b) Si en cada tramo disminuye su rapidez en un 80% ¿Cuánto demorarían en llegar
a Curicó?


c) Esta sección consiste en realizar una animación de cada participante del
entrenamiento, considera la elaboración de los sprites, los colores asignados de
cada persona y el escalamiento en el tiempo.


1) Elaborar una técnica sencilla de animación sobre un plano
2) Asignar un color a cada participante 
3) Desafío :: animar con una bicicleta por cada participante
